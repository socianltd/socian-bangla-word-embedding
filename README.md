# README #

Pretrained Bangla word embeddings trained on a large (5GB) corpus of web crawls. 
Currently, only word2vec model is available. But we will be adding more embeddings (GloVe, FastText) in the future.

We are crawling popular Bangla news and blog sites using Apache Nutch. And the corpus will be getting much larger in the next releases.


### File Description ###
1. bn.word2vec.txt
This file is a 100 dimensional word2vec model with 276776 unique tokens (in text format).
Can be easily loaded using gensim or any other tools.
For example - 
```python
from gensim.models.keyedvectors import KeyedVectors
word2vec_model = KeyedVectors.load_word2vec_format('bn.word2vec.txt', binary=False)
```

Or it can be directly loaded in Keras embedding layer. Follow this [Using pre-trained word embeddings in a Keras model](https://blog.keras.io/using-pre-trained-word-embeddings-in-a-keras-model.html)



Need to talk? tareq@socian.xyz